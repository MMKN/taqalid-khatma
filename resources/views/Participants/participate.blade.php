<!DOCTYPE html>
<html>
<head>
	<title> Participate </title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<h3> Participate by reading Goz2 #{{ $goz2 }} </h3>
		<form method="POST" action="{{ URL::route('post.participate' , [$khatma_id , $goz2]) }}" class="form-horizontal">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			 	<div class="form-group">
				    <label> Name </label>
				    <input type="text" name="name" class="form-control" placeholder="Name">
			 	</div>
				<div class="form-group">
			    	<label> Email address </label>
			    	<input type="email" name="email" class="form-control" placeholder="Email">
			 	</div>
				<div class="form-group">
					<input class="btn btn-primary" type="submit" value="Participate" />	
				</div>
		</form>
	</div>

</body>
</html>