<!DOCTYPE html>
<html>
<head>
	<title> Khatma </title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
	<p> Name  :  {{ $khatma['name'] }} </p>
	<p> Owner :  {{ $khatma['owner'] }} </p>
	<p> Added :  {{ $khatma['added'] }} </p>
	<table class="table table-striped table-bordered">
		<tr>
			<th> # </th>
			<th> Name </th>
			<th> Status </th>
			<th> Action </th>
		</tr>
		@foreach($khatma['participants'] as $participant)
			<tr>
				<td> {{ $participant['goz2'] }} </td>
				<td> {{ $participant['name'] }} </td>
				<td> {{ getGoz2Status($participant['status']) }} </td>
				<td> 
					@if ($participant['status'] == 0) 
						<a class="btn btn-primary btn-xs" href="{{ URL::route('get.participate' , [ $khatma['id'] , $participant['goz2']  ]) }}"> Participate </a>
					@elseif($participant['status'] == 1)
						<a class="btn btn-success btn-xs" href="{{ URL::route('get.finish' , [ $khatma['id'] , $participant['goz2']  ]) }}"> Fininsh </a>
					@else
						<a class="btn btn-warning btn-xs disabled">Finished</a>
					@endif
				</td>
			</tr>
		@endforeach
	</table>

	<script type="text/javascript">

	</script>
</body>
</html>

