<!DOCTYPE html>
<html>
<head>
	<title>Create Khatma</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<h3> Create New Khatma </h3>
		<form method="POST" action="{{ URL::route('khatma.store') }}" class="form-horizontal">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group">
				<label> Name of Khatma : </label>
				<input type="text" name="name" class="form-control" placeholder="name"/>
			</div>
			<div class="form-group">
				<input type="submit" value="Add" class="btn btn-primary"/>	
			</div>
		</form>
	</div>
</body>
</html>