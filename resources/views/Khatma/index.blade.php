<!DOCTYPE html>
<html>
<head>
	<title> Khatma </title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

	<table class="table table-striped table-bordered">
		<tr>
			<th> Name </th>
			<th> Owner </th>
			<th> Finished </th>
			<th> Added </th>
		</tr>
		@foreach($khatmat as $khatma)
			<tr>
				<td> <a href="{{ URL::route('khatma.show' , [$khatma['id']]) }}"> {{ $khatma['name'] }} </a> </td>
				<td> {{ $khatma['owner'] }} </td>
				<td> {{ $khatma['finished'] }} / 30 </td>
				<td> {{ $khatma['added'] }} </td>
			</tr>
		@endforeach
	</table>

</body>
</html>

