<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            a {
                color: #fff;
                text-decoration: none;
            }

            #btns {
                margin-top: 10px; 
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Welcome </div>
            </div>

            <div id="btns">
                <a class="btn btn-primary" href="{{ URL::route('khatma.create') }}"> Create New Khatma </a>
                &nbsp;
                <a class="btn btn-warning" href="{{ URL::route('khatma.index') }}"> List All Khatmat </a>
            </div>
            <div>
                
            </div>
        </div>
    </body>
</html>
