<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
    


$factory->define(Khatma\User\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});




$factory->define(Khatma\Khatma\Khatma::class, function (Faker\Generator $faker) {
	$user_ids = Khatma\User\User::lists('id')->toArray();
    return [
        'name' => $faker->name,
        'user_id' => $faker->randomElement($user_ids),
        'status' => 0,
    ];
});



$factory->define(Khatma\Participant\Participant::class, function (Faker\Generator $faker) {
    $khatmat_ids = Khatma\Khatma\Khatma::lists('id')->toArray();
    $agza2 = range(1, 30);
    $status = [0,1,2];

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'khatma_id' => $faker->randomElement($khatmat_ids),
        'goz2' => $faker->randomElement($agza2),
        'status' => $faker->randomElement($status),
        'session' => $faker->sha1,
    ];
});