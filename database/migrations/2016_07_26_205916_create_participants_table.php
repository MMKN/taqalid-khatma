<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->integer('khatma_id')->unsigned()->index();
            $table->integer('goz2');
            $table->integer('status'); // 0 available - 1 taken - 2 finished
            $table->string('session')->unique();
            $table->timestamps();

            $table->foreign('khatma_id')->references('id')->on('khatmat')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('participants');
    }
}
