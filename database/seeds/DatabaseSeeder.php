<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Khatma\Khatma\Khatma;
use Khatma\Participant\Participant;
use Khatma\User\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Model::unguard();
    	DB::statement("SET foreign_key_checks=0");

        User::truncate();
        Khatma::truncate();
    	Participant::truncate();
        
        factory(User::class , 10)->create();
        factory(Khatma::class , 50)->create();
        factory(Participant::class , 100)->create();

        DB::statement("SET foreign_key_checks=1");
        Model::reguard();
    }
}
