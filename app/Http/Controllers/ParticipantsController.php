<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Khatma\Participant\Commands\FinishGoz2Command;
use Khatma\Participant\Commands\ParticipateCommand;
use Khatma\Participant\Participant;
use Khatma\Participant\ParticipantRepository;
use Laracasts\Commander\CommanderTrait;

class ParticipantsController extends Controller
{

	use CommanderTrait;


	/**
	 * Participant repository instance.
	 * 
	 * @var Khatma\Participant\ParticipantRepository
	 */
	private $participant_repository;


	public function __construct(ParticipantRepository $participant_repository)
	{
		$this->participant_repository = $participant_repository;
	}
	

	/**
	 * Page where the visitor can participate to a khatma.
	 */
	public function getParticipate($khatma_id , $goz2)
	{
		$data['khatma_id'] = $khatma_id;
    	$data['goz2'] 	   = $goz2;

    	return View::make('Participants.participate' , $data);
	}


	/**
	 * Handle the participation request.
	 */
	public function postParticipate($khatma_id , $goz2)
	{
		Input::merge(['khatma_id' => $khatma_id , 'goz2' => $goz2]);

    	$validator = Validator::make(Input::all() , Participant::$rules);
    	if ($validator->fails()){ return $validator->messages(); }

		$this->execute(ParticipateCommand::class);

		return Redirect::route('khatma.show', [$khatma_id]);
	}


	/**
	 * Handle finishing goz2 request.
	 */
	public function getFinish($khatma_id , $goz2)
	{
		if (! $this->participant_repository->allowed($khatma_id , $goz2)){ return "You are not allowed to finish this Goz2"; }

		Input::merge(['khatma_id' => $khatma_id , 'goz2' => $goz2]);

		$this->execute(FinishGoz2Command::class);

		return Redirect::route('khatma.show', [$khatma_id]);
	}
}
