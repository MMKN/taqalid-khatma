<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Khatma\Khatma\Commands\AddKhatmaCommand;
use Khatma\Khatma\Khatma;
use Khatma\Khatma\KhatmaRepository;
use Khatma\Transformers\KhatmaTransformer;
use Khatma\User\UserRepository;
use Laracasts\Commander\CommanderTrait;

class KhatmatController extends Controller
{
	use CommanderTrait;


	/**
	 * Khatma repository instance.
	 *  
	 * @var Khatma\Khatma\KhatmaRepository
	 */
	private $khatma_repository;


	/**
	 * User repository instance.
	 * 
	 * @var Khatma\User\UserRepository
	 */
	private $user_repository;


	/**
	 * Khatma Transformer instance.
	 * 
	 * @var Khatma\Transformers\KhatmaTransformer
	 */
	private $khatma_transformer;


	public function __construct(KhatmaRepository $khatma_repository, KhatmaTransformer $khatma_transformer, UserRepository $user_repository)
	{
		$this->khatma_repository = $khatma_repository;
		$this->khatma_transformer = $khatma_transformer;
		$this->user_repository = $user_repository;
	}


	/**
	 * List all the unfinshed khatmat.
	 */
    public function index()
    {
    	$khatmat         = $this->khatma_repository->getUnFinishedKhatmat();
    	$data['khatmat'] = $this->khatma_transformer->transformCollection($khatmat);

        return View::make('Khatma.index', $data);
    }


    public function show($id)
    {
    	$khatma = $this->khatma_repository->find_by_id($id);
    	$data['khatma'] = $this->khatma_transformer->transform($khatma);

    	return View::make('Khatma.khatma' , $data);
    }

    /**
     * Creat khatma form page.
     */
    public function create()
    {
    	return View::make('Khatma.create');
    }


    /**
     * Add new khatma.
     */
    public function store()
    {
    	$user_id = $this->user_repository->getRandomUserId();
    	Input::merge(['user_id' => $user_id]); // Random user id 

    	$validator = Validator::make(Input::all() , Khatma::$rules);
    	if ($validator->fails()){ return $validator->messages(); }

    	$this->execute(AddKhatmaCommand::class);

    	return Redirect::route('khatma.index');
    }

}
