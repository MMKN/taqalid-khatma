<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	return View::make('welcome');
    return redirect()->route('khatma.show' , [1]);
});


Route::resource('khatma' , 'KhatmatController');
Route::controller('participants' , 'ParticipantsController' , [
	'getParticipate'  => 'get.participate',
	'postParticipate' => 'post.participate',
	'getFinish'       => 'get.finish',
]);
