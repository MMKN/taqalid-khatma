<?php 

	function getGoz2Status($status)
	{
		if($status == 0)      return "Avaliable";
		else if($status == 1) return "Taken";
		else                  return "Finished";
	}