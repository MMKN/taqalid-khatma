<?php

namespace Khatma\Transformers;

use Khatma\Khatma\KhatmaRepository;
use Khatma\Transformers\ParticipantTransformer;


class KhatmaTransformer extends Transformer
{

	private $khatma_repository;


	private $participant_transformer;



	public function __construct(KhatmaRepository $khatma_repository, ParticipantTransformer $participant_transformer)
	{
		$this->khatma_repository = $khatma_repository;
		$this->participant_transformer = $participant_transformer;
	}



	public function transform($item)
	{
		return [
			'id'  	   	   => $item['id'],
			'name'  	   => $item['name'],
			'owner' 	   => $this->khatma_repository->find_by_id($item['id'])->user->name,
			'finished' 	   => $this->khatma_repository->getFinishedAgza2($item['id']),
			'added' 	   => $item['created_at'],
			'participants' => $this->helper($item['id'] , $this->participant_transformer->transformCollection($this->khatma_repository->find_by_id($item['id'])->participants))
		];
	}


	public function helper($id , $participants)
	{
		$index  = 0;
		$agza2  = $this->khatma_repository->find_by_id($id)->participants->lists('goz2')->toArray();
		$result = [];

		for ($i = 1; $i <= 30 ; $i++) 
		{ 
			$participant = ['name' => '' , 'goz2' => $i , 'status' => 0];
			if (in_array($i , $agza2))
			{
				$participant['name']   = $participants[$index]['name'];
				$participant['goz2']   = (int)$participants[$index]['goz2'];
				$participant['status'] = $participants[$index]['status'];
				$index++;
			}
			array_push($result, $participant);
		}

		return $result;
	}
}