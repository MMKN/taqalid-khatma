<?php

namespace Khatma\Transformers;



class ParticipantTransformer extends Transformer
{
	public function transform($item)
	{
		return [
			'name'    => $item['name'],
			'email'   => $item['email'],
			'goz2' 	  => $item['goz2'],
			'status'  => $item['status'],
			'session' => $item['session']
		];
	}
}