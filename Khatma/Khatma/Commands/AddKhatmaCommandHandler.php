<?php

namespace Khatma\Khatma\Commands;

use Khatma\Khatma\Commands\AddKhatmaCommand;
use Khatma\Khatma\Khatma;
use Khatma\Khatma\KhatmaRepository;
use Laracasts\Commander\CommandHandler;

class AddKhatmaCommandHandler implements CommandHandler 
{

	/**
	 * Khatma repository instance.
	 *  
	 * @var Khatma\Khatma\KhatmaRepository
	 */
	private $khatma_repository;


	public function __construct(KhatmaRepository $khatma_repository)
	{
		$this->khatma_repository = $khatma_repository;
	}


    /**
     * Handle the command.
     *
     * @param AddKhatmaCommand $command
     */
    public function handle($command)
    {
    	$khatma = Khatma::createKhatma($command->name , $command->user_id);
    	$this->khatma_repository->save($khatma);
    }

}