<?php

namespace Khatma\Khatma\Commands;

class AddKhatmaCommand
{

    /**
     * @var string
     */
    public $name;


    /**
     * @var integer
     */
    public $user_id;


    /**
     * @param string $name
     * @param integer $user_id
     */
    public function __construct($name, $user_id)
    {
        $this->name = $name;
        $this->user_id = $user_id;
    }

}