<?php

namespace Khatma\Khatma;

use Khatma\Participant\Participant;


class KhatmaRepository 
{

	/**
	 * Find a khatma by id.
	 * 
	 * @param  integer $khatma_id
	 * 
	 * @return Khatma\Khatma\Khatma
	 */
	public function find_by_id($khatma_id)
	{
		return Khatma::find($khatma_id);
	}


	/**
	 * Get the unfinshed khatamat.
	 * 
	 * @return Collection
	 */
	public function getUnFinishedKhatmat()
	{
		return Khatma::latest()->where('status' , '0')->get();
	}


	/**
	 * Save khatma to DB.
	 * 
	 * @param  Khatma\Khatma\Khatma $khatma
	 */
	public function save(Khatma $khatma)
	{
		return $khatma->save();
	}


	/**
	 * Get the number of finihed agza2 of specific khatma.
	 * 
	 * @param  integer $khatma_id
	 */
	public function getFinishedAgza2($khatma_id)
	{
		return Participant::where('khatma_id' , $khatma_id)->where('status' , 2)->count();
	}
}