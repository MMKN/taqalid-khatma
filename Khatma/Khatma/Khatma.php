<?php

namespace Khatma\Khatma;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Khatma extends Model
{

	/**
	 * Table name.
	 */
    protected $table = 'khatmat';


    /**
     * Fillable fields.
     */
    protected $fillable = ['name' , 'user_id'];


    /**
     * Fields to be Carbon instances.
     */
    protected $dates = ['created_at' , 'updated_at'];


    /**
     * Default date format.
     */
    protected $dateFormat = 'Y-m-d H:i:s';


    
    public static $rules = [
        'name'    => 'required',
        'user_id' => 'required'
    ];

    /* ========================================= Relationships ============================================== */

    /**
     * Relationship : Get the user owns the khatma. 
     * 
     * @return Khatma\User\User
     */
    public function user()
    {
    	return $this->belongsTo('Khatma\User\User');
    }


    public function participants()
    {
        return $this->hasMany('Khatma\Participant\Participant')->orderBy('goz2', 'asc');
    }

    /* ========================================= Relationships ============================================== */

    /* ========================================= Functions ============================================== */

    /**
     * Create new Khatma object.
     * 
     * @param  string  $name
     * @param  integer $user_id
     * 
     * @return Khatma\Khatma\Khatma
     */
    public static function createKhatma($name , $user_id)
    {
    	return new static (compact('name' , 'user_id'));
    }

    /* ========================================= Functions ============================================== */

    /*
    public function getCreatedAtAttribute($created_at)
    {
        return Carbon::parse($created_at)->year;
    }
    */
}
