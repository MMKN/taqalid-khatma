<?php 

namespace Khatma\User;

use Khatma\User\User;


class UserRepository
{
	
	function getRandomUserId()
	{
		$users = User::lists('id')->toArray();
		return $users[array_rand($users)];
	}

}