<?php

namespace Khatma\Participant\Commands;

use Illuminate\Support\Facades\Session;
use Khatma\Participant\Participant;
use Khatma\Participant\ParticipantRepository;
use Laracasts\Commander\CommandHandler;

class ParticipateCommandHandler implements CommandHandler 
{	

	private $participant_repository;


	public function __construct(ParticipantRepository $participant_repository)
	{
		$this->participant_repository = $participant_repository;
	}


    /**
     * Handle the command.
     *
     * @param object $command
     * @return void
     */
    public function handle($command)
    {
    	$participant = Participant::createParticipant($command->name , $command->email , $command->khatma_id , $command->goz2);
    	$this->participant_repository->save($participant);
    	Session::put('key', $participant->session);
    }

}