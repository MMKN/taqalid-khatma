<?php

namespace Khatma\Participant\Commands;

class FinishGoz2Command 
{

    /**
     * @var string
     */
    public $khatma_id;

    /**
     * @var string
     */
    public $goz2;

    /**
     * @param string $khatma_id
     * @param string $goz2
     */
    public function __construct($khatma_id, $goz2)
    {
        $this->khatma_id = $khatma_id;
        $this->goz2 = $goz2;
    }

}