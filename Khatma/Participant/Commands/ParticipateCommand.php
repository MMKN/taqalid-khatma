<?php

namespace Khatma\Participant\Commands;

class ParticipateCommand 
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $khatma_id;

    /**
     * @var string
     */
    public $goz2;

    /**
     * @param string $name
     * @param string $email
     * @param string $khatma_id
     * @param string $goz2
     */
    public function __construct($name, $email, $khatma_id, $goz2)
    {
        $this->name = $name;
        $this->email = $email;
        $this->khatma_id = $khatma_id;
        $this->goz2 = $goz2;
    }

}