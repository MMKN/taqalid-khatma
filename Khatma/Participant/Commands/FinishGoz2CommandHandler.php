<?php

namespace Khatma\Participant\Commands;

use Khatma\Participant\ParticipantRepository;
use Laracasts\Commander\CommandHandler;

class FinishGoz2CommandHandler implements CommandHandler 
{	

	/**
	 * Participant repository instance.
	 * 
	 * @var Khatma\Participant\ParticipantRepository
	 */
	private $participant_repository;


	public function __construct(ParticipantRepository $participant_repository)
	{
		$this->participant_repository = $participant_repository;
	}


    /**
     * Handle the command.
     *
     * @param object $command
     * @return void
     */
    public function handle($command)
    {
    	$participant = $this->participant_repository->getParticipant($command->khatma_id , $command->goz2);
    	$participant = $this->participant_repository->finish_goz2($participant);
    	return $this->participant_repository->save($participant);

    	// here we can check if the khatma is finished or not 
    	// to close it and send mail to the user
    }
}