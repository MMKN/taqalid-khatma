<?php

namespace Khatma\Participant;

use Illuminate\Support\Facades\Session;
use Khatma\Participant\Participant;


class ParticipantRepository 
{

	/**
	 * Get participant by khatma_id and goz2.
	 * 
	 * @param  integer $khatma_id
	 * @param  integer $goz2
	 * 
	 * @return Khatma\Participant\Participant
	 */
	public function getParticipant($khatma_id , $goz2)
	{
		return Participant::where('khatma_id' , $khatma_id)->where('goz2' , $goz2)->first();
	}


	/**
	 * Save Participant to DB.
	 * 
	 * @param Khatma\Participant\Participant
	 */
	public function save(Participant $participant)
	{
		return $participant->save();
	}


	/**
	 * Check if the participate is allowed to finish this goz2 or not.
	 * 
	 * @param  integer $khatma_id
	 * @param  integer $goz2
	 * 
	 * @return boolean
	 */
	public function allowed($khatma_id , $goz2)
	{
		return $this->participantSession($khatma_id , $goz2) == Session::get('key');
	}


	/**
	 * Get the participate session from DB.
	 * 
	 * @param  integer $khatma_id [description]
	 * @param  integer $goz2      [description]
	 */
	public function participantSession($khatma_id , $goz2)
	{
		return $this->getParticipant($khatma_id , $goz2)->session;
	}


	/**
	 * Change the status of the goz2 to finished.
	 * 
	 * @param Khatma\Participant\Participant $participant
	 * 
	 * @return Khatma\Participant\Participant
	 */
	public function finish_goz2(Participant $participant)
	{
		$participant->status = 2;
		return $participant;
	}

}