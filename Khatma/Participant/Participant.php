<?php

namespace Khatma\Participant;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    /**
	 * Table name.
	 */
    protected $table = 'participants';


    /**
     * Fillable fields.
     */
    protected $fillable = ['name' , 'email' , 'khatma_id' , 'goz2' , 'status' , 'session'];


    /**
     * Fields to be Carbon instances.
     */
    protected $dates = ['created_at' , 'updated_at'];


    /**
     * Default date format.
     */
    protected $dateFormat = 'Y-m-d H:i:s';


    public static $rules = [
        'name'      => 'required',
        'email'     => 'required',
        'goz2'      => 'required',
        'khatma_id' => 'required'
    ];

    /* ========================================= Relationships ============================================== */



    /* ========================================= Relationships ============================================== */


    /* ========================================= Functions ============================================== */

    /**
     * Create new Participant object.
     *  
     * @param  string  $name
     * @param  strin  $email
     * @param  integer  $khatma_id
     * @param  integer  $goz2
     * @param  integer $status
     * 
     * @return Khatma\Participant\Participant
     */
    public static function createParticipant($name , $email , $khatma_id , $goz2 , $status = 1)
    {
    	$faker = Faker::create();
    	$session = $faker->sha1;
    	return new static (compact('name' , 'email' , 'khatma_id' , 'goz2' , 'status' , 'session'));
    }

    /* ========================================= Functions ============================================== */

}
